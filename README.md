### Fake Instagram Project

### Contributors:

- 1. Maxym Kotov

- 2. Luka Ponomarenko

- 3. Danyl Sherbakov

### Tasks:

- 1. Main feed posts & infinite scroll

- 2. Post modal & backend

- 3. User profile & recommendations / subscriptions

### [Link](https://fake-instagram-danit.herokuapp.com/)

### To launch:

- In root folder: npm run build

- After: npm start

- Open browser on localhost:4500
