import React, { useEffect, useState } from "react";
import styles from "./UserLink.module.scss";
import Nickname from "../Nickname/Nickname";
import Avatar from "../Avatar/Avatar";
import axios from "axios";

const UserLink = ({ nick, addRules, stylednick, style }) => {
  const [image, setImage] = useState("/guest.png");
  useEffect(() => {
    const getUser = async () => {
      const image = await axios(`/user/${nick}`);
      setImage(image.data.image);
    };

    getUser();
  }, [nick]);

  return (
    <div className={styles.container} style={addRules}>
      <Avatar image={image} styles={style} />
      <Nickname name={nick} style={stylednick} />
    </div>
  );
};

export default UserLink;
