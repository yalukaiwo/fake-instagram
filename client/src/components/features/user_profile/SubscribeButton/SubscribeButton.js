import React from "react";
import styles from "./SubscribeButton.module.scss";

function SubscribeButton({ isSubscribed, onClick, isSubscribing }) {
  return isSubscribed ? (
    <button
      className={styles.unsubscribeButton}
      onClick={onClick}
      disabled={isSubscribing}
    >
      Unsubscribe
    </button>
  ) : (
    <button
      className={styles.button}
      onClick={onClick}
      disabled={isSubscribing}
    >
      Subscribe
    </button>
  );
}

export default SubscribeButton;
