import React, { useState } from "react";
import ShortcutInfo from "../ShortcutInfo/ShortcutInfo";
import classes from "./ProfilePostShortcut.module.scss";
import PostModal from "../../post_modal/PostModal/PostModal";

const ProfilePostShortcut = ({ post }) => {
  const [isHovered, setHovered] = useState(false);
  const [modalStatus, setModalStatus] = useState(false);

  return (
    <>
      <div
        className={classes.shortcut}
        onMouseOver={() => setHovered(true)}
        onMouseLeave={() => setHovered(false)}
        onClick={() => setModalStatus(true)}
      >
        <img
          className={classes.shortcutImage}
          src={post.image}
          alt="shortcut"
        ></img>
        {isHovered && (
          <ShortcutInfo likes={post.likes} comments={post.comments} />
        )}
      </div>
      {modalStatus && (
        <PostModal post={post} closeModal={() => setModalStatus(false)} />
      )}
    </>
  );
};

export default ProfilePostShortcut;
