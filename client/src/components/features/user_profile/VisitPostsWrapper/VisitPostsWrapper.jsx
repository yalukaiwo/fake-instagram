import React from "react";
import styles from "./VisitPostsWrapper.module.scss";
import ProfilePostShortcut from "../ProfilePostShortcut/ProfilePostShortcut";
import Loader from "../../main_feed/Loader/Loader";

const VisitPostsWrapper = (props) => {
  if (props.isLoading) return <Loader />;

  if (!props.posts[0]) {
    return (
      <div className={styles.noPostWrapper}>
        <h1 className={styles.noPost}>This user does not seem to have posts</h1>
      </div>
    );
  }

  return (
    <div className={styles.wrapper}>
      {props.posts.map((el) => (
        <ProfilePostShortcut post={el} key={el._id} />
      ))}
    </div>
  );
};

export default VisitPostsWrapper;
