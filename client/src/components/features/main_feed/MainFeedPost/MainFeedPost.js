import React, { useState } from "react";
import { connect } from "react-redux";
import { toggleLike } from "../../../../store/main_page/operations";
import LikeIcon from "../../../common/LikeIcon/LikeIcon";
import AddComment from "../../../common/AddComment/AddComment";
import Comments from "../Comments/Comments";
import styles from "./MainFeedPost.module.scss";
import UserLink from "../../../common/UserLink/UserLink";
import { Link } from "react-router-dom";
import Nickname from "../../../common/Nickname/Nickname";

export const MainFeedPost = ({ post, username, toggleLike }) => {
  const [isLikedPost, setIsLikedPost] = useState(post.likes.includes(username));
  const [likesAmount, setLikesAmount] = useState(post.likes.length);

  const handleLikeClick = () => {
    setIsLikedPost((prev) => !prev);
    setLikesAmount((prev) => (isLikedPost ? prev - 1 : prev + 1));
    toggleLike({ postId: post._id, username });
  };

  return (
    <article className={styles.container}>
      <header className={styles.header}>
        <Link to={`/profile/${post.author}`}>
          <UserLink
            ava={post.image}
            nick={post.author}
            addRules={{ marginLeft: "15px" }}
            stylednick={{
              marginLeft: "15px",
              fontWeight: 600,
              fontSize: "16px",
            }}
          />
        </Link>
      </header>
      <main className={styles.main}>
        <img
          onDoubleClick={handleLikeClick}
          className={styles.main__image}
          src={post.image}
          alt="Post"
          data-testid="post-image"
        />
      </main>
      <footer className={styles.footer}>
        <section className={styles.footer__actions}>
          <LikeIcon isLiked={isLikedPost} onClick={handleLikeClick} />
        </section>
        <section className={styles.footer__meta}>
          <p className={styles.footer__likesAmount}>
            {likesAmount} users liked this post
          </p>
          <div className={styles.footer__author}>
            <Link to={`/profile/${post.author}`}>
              <Nickname
                name={post.author}
                style={{ fontWeight: 600, fontSize: "16px" }}
              />
            </Link>
            <span className={styles.footer__description}>
              {post.description}
            </span>
          </div>
        </section>
        <section className={styles.footer__comments}>
          <Comments comments={post.comments} />
          <AddComment postId={post._id} username={username} />
        </section>
      </footer>
    </article>
  );
};

export default connect((state) => ({ username: state.user.username }), {
  toggleLike,
})(MainFeedPost);
