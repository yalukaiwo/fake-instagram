import React, { useState } from "react";
import styles from "./Comments.module.scss";
import Nickname from "../../../common/Nickname/Nickname";

export default function Comments({ comments: temp }) {
  const [areShown, setAreShown] = useState(false);
  const comments = [...temp].reverse();

  if (!comments.length) return <div className={styles.container} />;

  const shownComments = areShown ? comments : [comments[0]];

  const shownCommentsElements = shownComments.map((item, index) => (
    <div className={styles.comment} key={index} data-testid="comment">
      <Nickname
        name={item.author}
        style={{ fontWeight: 600, fontSize: "14px" }}
      />
      <span className={styles.comment__text}>{item.message}</span>
    </div>
  ));

  return (
    <div className={styles.container}>
      {shownCommentsElements}
      {!areShown && comments.length > 1 && (
        <p
          className={styles.toggler}
          onClick={() => setAreShown(true)}
          data-testid="comment-toggler"
        >
          Show all
        </p>
      )}
    </div>
  );
}
