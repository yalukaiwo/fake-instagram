import { PostModal } from "./PostModal";
const { render } = require("@testing-library/react");
const { default: userEvent } = require("@testing-library/user-event");

const params = {
  closeModal: jest.fn(),
  addComment: jest.fn(),
  likePost: jest.fn(),
  image: "blablabla",
  username: "yalukaiwo",
  post: {
    author: "yalukaiwo",
    comments: [{ author: "yalukaiwo", message: "blablabla" }],
    likes: ["yalukaiwo"],
    image:
      "https://res.cloudinary.com/yalukaiwo/image/upload/v1633055377/avatars/6156728bd2b59ab4f2910c38.png",
  },
};

describe("Modal tests", () => {
  test("Smoke test", () => {
    render(
      <PostModal
        post={params.post}
        addComment={params.addComment}
        likePost={params.likePost}
        closeModal={params.closeModal}
      />
    );
  });
  test("Like button test", () => {
    const likePost = jest.fn();
    const { getByTestId } = render(
      <PostModal
        post={params.post}
        addComment={params.addComment}
        likePost={likePost}
        closeModal={params.closeModal}
      />
    );
    userEvent.dblClick(getByTestId("postImage"));
    expect(likePost).toHaveBeenCalledTimes(1);
    userEvent.click(getByTestId("liked-icon"));
    expect(likePost).toHaveBeenCalledTimes(2);
  });
});
