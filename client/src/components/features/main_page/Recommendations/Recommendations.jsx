import React from "react";
import styles from "./Recommendations.module.scss";
import { connect } from "react-redux";
import RecommendedUser from "../RecommendedUser/RecommendedUser";

const Recommendations = ({recommendations, username, subscribed}) => {
  if (!recommendations.length) return null;

  return (
    <div className={styles.container}>
      <h3 className={styles.title}>Recommendations</h3>
      <div className={styles.wrapper}>
        {recommendations.map(user => (
          <RecommendedUser
            key={user.username}
            user={user}
            currentUserName={username}
            subscribed={subscribed}
          />
        )).splice(0, 5)}
      </div>
    </div>
  );
};

const mapStateToProps = (state) => ({
  recommendations: state.user.recommendations,
  subscribed: state.user.subscribed,
  visitedPage: state.visitedPage,
  username: state.user.username,
});

export default connect(mapStateToProps)(Recommendations);