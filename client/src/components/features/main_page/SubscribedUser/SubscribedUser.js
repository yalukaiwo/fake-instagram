import React from "react";
import { NavLink } from "react-router-dom";
import UserLink from "../../../common/UserLink/UserLink";

function SubscribedUser({user}) {
  return (
    <NavLink exact to={`/profile/${user.username}`}>
      <UserLink
        ava={user.image || "./guest.png"}
        nick={user.username}
        stylednick={{
          fontWeight: 600,
          fontSize: "14px",
          marginLeft: "10px",
        }}
        style={{
          flexGrow: 0,
          marginRight: "3px",
        }}
      />
    </NavLink>
  );
}

export default SubscribedUser;
