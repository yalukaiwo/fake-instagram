import React from "react";
import { connect } from "react-redux";
import { NavLink } from "react-router-dom";
import { subscribeMain } from "../../../../store/user/operations";
import UserLink from "../../../common/UserLink/UserLink";
import ShortSubscribeButton from "../ShortSubscribeButton/ShortSubscribeButton";
import styles from "./RecommendedUser.module.scss";

export const RecommendedUser = ({user, currentUserName, subscribed, subscribeMain}) => {
  return (
    <div className={styles.item}>
      <NavLink exact to={`/profile/${user.username}`}>
        <UserLink
          ava={user.image || "./guest.png"}
          nick={user.username}
          stylednick={{
            fontWeight: 600,
            fontSize: "14px",
            marginLeft: "10px",
          }}
          style={{
            flexGrow: 0,
            marginRight: "3px",
          }}
        />
      </NavLink>
      <ShortSubscribeButton
        user={user.username}
        onClick={() => {
          subscribeMain({
            username: user.username,
            aunt: currentUserName,
            subscribed,
          });
        }}
      />
    </div>
  );
};

export default connect(null, { subscribeMain })(RecommendedUser);
