import React from "react";
import Recommendations from "../Recommendations/Recommendations";
import Subscriptions from "../Subscriptions/Subscriptions";
import classes from "./SidePanel.module.scss";

export default function SidePanel() {
  return (
    <div className={classes.container}>
      <Subscriptions />
      <Recommendations />
    </div>
  );
}
