import React from "react";
import { connect } from "react-redux";
import styles from "./ShortSubscribeButton.module.scss";

export const ShortSubscribeButton = ({ user, subscribed, onClick }) => {
  const isSubscribed = subscribed.includes(user);

  return isSubscribed ? (
    <button className={styles.unsubscribeButton} onClick={onClick}>
      Following
    </button>
  ) : (
    <button className={styles.button} onClick={onClick}>
      Follow
    </button>
  );
};

const mapStateToProps = (state) => ({
  subscribed: state.user.subscribed,
});

export default connect(mapStateToProps)(ShortSubscribeButton);
