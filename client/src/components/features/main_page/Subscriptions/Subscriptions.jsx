import React, { useEffect, useState } from "react";
import { connect } from "react-redux";
import styles from "./Subscriptions.module.scss";
import axios from "axios";
import SubscribedUser from "../SubscribedUser/SubscribedUser";
import Loader from "../../main_feed/Loader/Loader";

const Subscriptions = ({subscribed}) => {
  const [usersInfo, setUsersInfo] = useState([]);
  const [isLoading, setLoading] = useState(true);

  useEffect(() => {
    const getUsers = async () => {
      if (!subscribed.length) return;

      const res = await Promise.all(
        subscribed.map(async (nickname) => {
          return await axios(`/user/${nickname}`);
        })
      );

      setUsersInfo(res.map((object) => object.data));
      setLoading(false);
    };

    getUsers();
  }, [subscribed, setUsersInfo]);

  if (!subscribed.length) return null;

  const SubscribedUsers = usersInfo.map(user => <SubscribedUser key={user.username} user={user} />);

  return (
    <div className={styles.container}>
      <h3 className={styles.title}>Subscriptions</h3>
      <div className={styles.wrapper}>
        {isLoading ? <Loader /> : SubscribedUsers}
      </div>
    </div>
  );
};

const mapStateToProps = (state) => ({
  subscribed: state.user.subscribed
});

export default connect(mapStateToProps)(Subscriptions);
