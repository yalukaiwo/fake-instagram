import React, { useEffect, useState } from "react";
import { connect } from "react-redux";
import { useParams } from "react-router-dom";
import {
  checkIfSubscribed,
  getPosts,
  getUser,
  subscribe,
} from "../../store/visited_page/operations";
import styles from "./Profile.module.scss";
import Nickname from "../../components/common/Nickname/Nickname";
import VisitPostsWrapper from "../../components/features/user_profile/VisitPostsWrapper/VisitPostsWrapper";
import Avatar from "../../components/common/Avatar/Avatar";
import SubscribeButton from "../../components/features/user_profile/SubscribeButton/SubscribeButton";
import GlobalLoader from "../../components/common/GlobalLoader/GlobalLoader.jsx";
import { updateSubscriptions } from "../../store/user/operations";

const Profile = ({
  getUser,
  getPosts,
  currentUserName,
  visitedPage,
  subscribe,
  isSubscribed,
  isSubscribing,
  updateSubscriptions
}) => {
  const { username } = useParams();
  const [isLoading, setLoading] = useState(true);
  const [postsLoading, setPostsLoading] = useState(true);

  useEffect(() => {
    if (currentUserName) {
      checkIfSubscribed({ username, currentUserName });
    }

    getUser(username).then(() => {
      setLoading(false);
    });
    getPosts(username).then(() => {
      setPostsLoading(false);
    });
  }, [getPosts, getUser, currentUserName, username, setLoading]);

  if (isLoading) {
    return <GlobalLoader image="../favicon.ico" />;
  }

  const onSubscribeClick = async () => {
    const response = (await subscribe({
      username,
      aunt: currentUserName,
      subs: visitedPage.subscribers,
    })).payload;

    updateSubscriptions(response);
  }

  return (
    <main className={styles.main}>
      <div className={styles.container}>
        <header className={styles.avaHeader}>
          <Avatar image={visitedPage.image || "../guest.png"} />
          <section className={styles.avaInfoSection}>
            <div className={styles.subscribeSettings}>
              <Nickname name={visitedPage.username} />
              {!(username === currentUserName) && (
                <SubscribeButton
                  isSubscribing={isSubscribing}
                  onClick={onSubscribeClick}
                  isSubscribed={isSubscribed}
                />
              )}
            </div>
            <ul className={styles.stats}>
              <li className={styles.statElement}>
                <span> {visitedPage.posts.length}</span> posts
              </li>
              <li className={styles.statElement}>
                Subscribers: <span> {visitedPage.subscribers.length}</span>
              </li>
              <li className={styles.statElement}>
                Subscribed: <span> {visitedPage.subscribed.length}</span>
              </li>
            </ul>
            <div className={styles.descriptionWrapper}>
              <p className={styles.descriptionWrapper}>
                {visitedPage.description}
              </p>
            </div>
          </section>
        </header>
        <div className={styles.postContent}>
          <VisitPostsWrapper
            posts={visitedPage.posts}
            isLoading={postsLoading}
          />
        </div>
      </div>
    </main>
  );
};

const mapStateToProps = (state) => ({
  visitedPage: state.visitedPage,
  currentUserName: state.user.username,
  isSubscribing: state.visitedPage.isSubscribing,
  isSubscribed: state.visitedPage.subscribers.includes(state.user.username),
});

export default connect(mapStateToProps, {
  getUser,
  getPosts,
  subscribe,
  checkIfSubscribed,
  updateSubscriptions
})(Profile);
