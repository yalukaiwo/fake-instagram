import React from "react";
import { connect } from "react-redux";
import GlobalLoader from "../../components/common/GlobalLoader/GlobalLoader";
import MainFeed from "../../components/features/main_feed/MainFeed/MainFeed";
import SidePanel from "../../components/features/main_page/SidePanel/SidePanel";
import styles from "./Main.module.scss";

export const Main = ({ isLoading }) => {
  return (
    <>
      {isLoading && <GlobalLoader image="/favicon.ico" />}
      <div className={styles.globalWrapper}>
        <MainFeed />
        <div className={styles.recWrapper}>
          <SidePanel />
        </div>
      </div>
    </>
  );
};

export default connect((state) => ({ isLoading: state.mainPage.isLoading }))(
  Main
);
