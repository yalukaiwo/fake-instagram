import { createSlice } from "@reduxjs/toolkit";
import { authenticate, getRecommendations, subscribeMain, updateSubscriptions } from "./operations";

const initialState = {
  image: "",
  username: "",
  description: "",
  subscribers: [],
  posts: [],
  subscribed: [],
  recommendations: [],
};

export const userSlice = createSlice({
  name: "user",
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder
      .addCase(authenticate.fulfilled, (state, action) => {
        Object.assign(state, action.payload);
      })
      .addCase(getRecommendations.fulfilled, (state, action) => {
        state.recommendations = action.payload;
      })
      .addCase(subscribeMain.fulfilled, (state, action) => {
        const { subscribed, username, status } = action.payload;
        if (!status) {
          const copySubscribed = [...subscribed];
          copySubscribed.splice(copySubscribed.indexOf(username), 1);
          state.subscribed = copySubscribed;
        } else {
          state.subscribed = [...subscribed, username];
        }
      })
      .addCase(updateSubscriptions.fulfilled, (state, action) => {
        const {status, username, userInfo} = action.payload;
        
        if (status) {
          state.subscribed.push(username);

          const userIndex = state.recommendations.findIndex(user => user.username === username);
          state.recommendations.splice(userIndex, 1);
        } else {
          state.recommendations.push(userInfo);
          state.subscribed.splice(state.subscribed.indexOf(username), 1);
        }
      });
  },
});

export default userSlice.reducer;
