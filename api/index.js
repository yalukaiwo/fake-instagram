const express = require("express");
const dotenv = require("dotenv");
const mongoose = require("mongoose");
const mainRouter = require("./handlers/main");
const postRouter = require("./handlers/post");
const userRouter = require("./handlers/user");
const cors = require("cors");
const fileUpload = require("express-fileupload");

dotenv.config();
const port = process.env.PORT || 4500;
const databaseConnectionKey =
  "mongodb+srv://website_connection:Conn1808@api.wwonz.mongodb.net/data_storage?retryWrites=true&w=majority";

const app = express();
app.use(fileUpload());
app.use(express.static(__dirname + "/public"));
app.use(express.json());

const corsOptions = {
  origin: "*",
  credentials: true,
  optionSuccessStatus: 200,
};

app.use(cors(corsOptions));

// breakline (start)

app.get("/", (req, res) => {
  res.sendFile(__dirname + "/public/index.html");
});

app.use("/", mainRouter);
app.use("/post", postRouter);
app.use("/user", userRouter);

app.get("*", (req, res) => {
  res.sendFile(__dirname + "/public/index.html");
});

// breakline (end)

const startServer = () => {
  app.listen(port);
};

const connectToDatabase = () => {
  const properties = {
    useNewUrlParser: true,
  };
  mongoose.connect(databaseConnectionKey, properties);
  return mongoose.connection;
};

const launch = () => {
  connectToDatabase().once("open", startServer);
  console.log("Server started with no errors");
};

launch();
